import logging
import os
from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from abc import ABC, abstractmethod


class Reporter(ABC):
    def __init__(self, template_name):
        self.template_name = template_name
        self.logger = logging.getLogger(self.__class__.__name__)
        self.output_filename = self._get_output_filename()

    @abstractmethod
    def _get_output_filename(self):
        pass

    @abstractmethod
    def _get_template_vars(self):
        pass

    def generate_report(self):
        env = Environment(loader=FileSystemLoader(os.path.join(Path(__file__).parents[1], 'templates')))
        template = env.get_template(self.template_name)
        html_output = template.render(self._get_template_vars())
        self._write_report_to_file(html_output)

    def _write_report_to_file(self, html_output):
        if os.path.isfile(self.output_filename):
            self.logger.warning("Report output file already exists! The file {} will be overwritten."
                                .format(self.output_filename))
        with open(self.output_filename, 'w') as file:
            file.write(html_output)
