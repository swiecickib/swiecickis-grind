from argparse import ArgumentTypeError
from distutils.util import strtobool
from multiprocessing import cpu_count


def check_positive_float(val):
    val = float(val)
    if val <= 0:
        raise ArgumentTypeError('{} is not a positive float. Please try again with a positive float.'.format(val))
    return val


def check_positive_int(val):
    val = int(val)
    if val <= 0:
        raise ArgumentTypeError('{} is not a positive int. Please try again with a positive int.'.format(val))
    return val


def check_processes_input(val):
    val = int(val)
    if val <= 0 or val > cpu_count():
        raise ArgumentTypeError('{} is not a valid value for processes. The number of processes must be greater than 0 '
                                'and less than or equal to the number of cpu cores available on your platform which '
                                'is {}'.format(val, cpu_count()))
    return val


def str_to_bool(val):
    try:
        return bool(strtobool(val))
    except ValueError:
        raise ArgumentTypeError('{} is not a valid value. Boolean value is expected.'.format(val))
