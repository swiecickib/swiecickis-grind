import argparse
import sys
from multiprocessing import cpu_count
from initializer.init_utils import check_positive_int, check_positive_float, check_processes_input, str_to_bool


class InitHelper:
    def __init__(self):
        self.params = None
        self._parser = self._get_parser()

    def initialize(self):
        """If command line args are provided they will be parsed. Otherwise input will be retrieved interactively."""
        if len(sys.argv) == 1:
            print(self._get_intro())
            interactive_args = self._get_interactive_user_input()
            args = self._parser.parse_args(interactive_args)
        else:
            args = self._parser.parse_args()
        self._process_args(args)

    def _process_args(self, args):
        if args.table_type == 'a':
            args.table_type = 'american'
        if args.table_type == 'e':
            args.table_type = 'european'
        self._validate_args(args)
        self.params = args

    def _get_interactive_user_input(self):
        interactive_args = list()
        for action in self._parser._actions:
            if isinstance(action, argparse._StoreAction):
                prompt_str = action.help
                if action.choices is not None:
                    prompt_str += ' You have the following choices: {}'.format(action.choices)
                prompt_str += '\n: '
                self._get_user_input_for_action(action, prompt_str, interactive_args)
        return [str(i) for i in interactive_args]

    def _get_user_input_for_action(self, action, prompt_str, interactive_args):
        action_input = input(prompt_str)
        if action_input == '' and action.default is not None:
            if not action.required:
                interactive_args.append('--{}'.format(action.dest))
            interactive_args.append(action.default)
        else:
            try:
                action_input = action.type(action_input)
                if action.choices is not None and action_input not in action.choices:
                    raise ValueError('{} was not one of the allowed choices: {}. Please try again.'
                                     .format(action_input, action.choices))
                if not action.required:
                    interactive_args.append('--{}'.format(action.dest))
                interactive_args.append(action_input)
            except Exception as e:
                print(e)
                self._get_user_input_for_action(action, prompt_str, interactive_args)

    def _get_parser(self):
        parser = argparse.ArgumentParser(description=self._get_intro(),
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('strategy', type=str.lower, choices=['grind', 'gale'],
                            help='Select which strategy you\'d like to research.')
        parser.add_argument('table_type', type=str.lower, choices=['a', 'american', 'e', 'european'],
                            help='Specify the type of table for processing. European roulette has one zero and '
                                 'American roulette has two zeros.')
        parser.add_argument('min_bet', type=check_positive_float,
                            help='Specify the minimum bet allowed where you\'d like to research betting.')
        parser.add_argument('max_bet', type=check_positive_float,
                            help='Specify the maximum bet allowed where you\'d like to research betting.')
        parser.add_argument('min_bet_increment', type=check_positive_float,
                            help='Specify the minimum amount by which you are able to increase your bet. For the gale '
                                 'strategy you should put the minimum denomination at the table. For the grind strategy'
                                 ' this is how much you want to increment your bet by each time a loss occurs.')
        parser.add_argument('winning_positions', type=check_positive_int,
                            help='Specify the number of positions you want to research betting on. For example, if you '
                                 'wanted to research betting on columns you would enter 12.')
        parser.add_argument('payout_factor', type=check_positive_int,
                            help='Specify the payout factor if the ball is to land on one of your winning positions. '
                                 'For example if you wanted to research betting on corners you would enter 8 since '
                                 'winning a corner bet pays 8 to 1.')
        parser.add_argument('--sim_count', type=check_positive_int, default=10**5,
                            help='OPTIONAL argument if using the \'grind\' strategy. This will set the number of times '
                                 'that the simulator will simulate a sequence of bets to gain a profit. DEFAULT: '
                                 '100,000')
        parser.add_argument('--bankrupt_at_max_bet', type=str_to_bool, default=True,
                            help='OPTIONAL argument if using the \'grind\' strategy. When set to true a simulation of '
                                 'the grind strategy will end if the max bet is played and lost. DEFAULT: True.')
        parser.add_argument('--bankrupt_level', type=check_positive_float, default=10**5,
                            help='OPTIONAL argument if using the \'grind\' strategy. Not utilized if '
                                 'bankrupt_at_max_bet is set to True. If the sum of bets for a sequence exceeds this '
                                 'amount then it will be considered an unrecoverable loss and recorded as such. '
                                 'DEFAULT: 100,000')
        parser.add_argument('--processes', type=check_processes_input, default=cpu_count(),
                            help='OPTIONAL argument if using the \'grind\' strategy. Dictates the number of processes '
                                 'utilized when running simulations in parallel. Default and max value are the '
                                 'number of cpu cores available on your platform which is {}'.format(cpu_count()))
        parser.add_argument('--log', type=str.upper, default='WARNING',
                            choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                            help='Set the log level. DEFAULT: WARNING.')
        # Might want to add extra parameters for customizing the output reporting.
        return parser

    @staticmethod
    def _get_intro():
        return '''
        Welcome to the Swiecicki's Grind Roulette Strategy Generator, Simulator, and Analyzer.\n\n
        
        There are two main ways to utilize this application. You can fill out the positional and optional command line
        arguments as defined below, or you can just run 'python swiecickis_grind.py' and enter the command line 
        arguments interactively with input prompts.\n\n
        
        If this is your first time using the application it is strongly encouraged that you first run it with no command
        line arguments so you can enter in the input arguments interactively. \n\n
        
        There are two different strategies you can research using this application.\n\n
        
        The first is the Martingale betting system and this can be performed by setting 'gale' as the first positional 
        argument. The application first determines the maximum number of profitable rolls which can be accomplished
        given the input parameters and then it finds the bet sequence of that length with the highest expected value
        sum for winning rolls. For more information on the Martingale strategy please read 
        https://en.wikipedia.org/wiki/Martingale_(betting_system)
        
        The second strategy you can research using this application is Oscar's Grind and it can be done by setting
        'grind' as the first positional argument. The Oscar's Grind betting strategy dictates that you bet the same
        amount when you lose and increment your bet when you win until you finish a betting session with a profit.
        One caveat is that if you could bet less to still finish a session with a profit you are commended to do so.
        There is an indefinite number of ways an Oscar's grind session could unfold, so it is best suited for analysis
        via monte carlo simulations. This application will simulate Oscar's grind using multiprocessing as many times
        as you tell it to using the input arguments. You can also set the betting sessions to end at a certain bankroll
        limit or when the max bet is reached. For more information on the Oscar's grind betting strategy please read
        https://en.wikipedia.org/wiki/Oscar%27s_grind
        
        When the application finishes running it will provide the user with a path to an output report. You can
        take the path to an output report and open it in a web browser to analyze the statistical properties of the
        strategy you generated or the simulations you ran.
        '''

    def _validate_args(self, args):
        if args.max_bet < args.min_bet:
            self._parser.error('The maximum bet must be a value greater than the minimum bet.')
        if args.table_type == 'american' and args.winning_positions > 38:
            self._parser.error('It is impossible to have greater than 38 winning positions at an American roulette '
                               'table.')
        if args.table_type == 'european' and args.winning_positions > 37:
            self._parser.error('It is impossible to have greater than 37 winning positions at a European roulette '
                               'table.')
