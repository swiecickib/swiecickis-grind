from collections import Counter


class SimResults:
    def __init__(self, sim_runners=None):
        # TODO: Look into putting data into a df to start or after combining.
        self.profits = Counter()
        self._busted_profits = None
        self._profited_profits = None
        self.profited_terminal_bets = Counter()
        self.busted_terminal_bets = Counter()
        self.profited_bet_counts = Counter()
        self.busted_bet_counts = Counter()
        self.profited_bankrolls = Counter()
        self.busted_bankrolls = Counter()
        self.longest_win_series_events = list()
        self.longest_lose_series_events = list()
        if isinstance(sim_runners, list):
            self._combine_sim_runners(sim_runners)

    @property
    def busted_profits(self):
        if self._busted_profits is None:
            self._busted_profits = Counter(dict([pair for pair in list(self.profits.items()) if pair[0] <= 0]))
        return self._busted_profits

    @property
    def profited_profits(self):
        if self._profited_profits is None:
            self._profited_profits = Counter(dict([pair for pair in list(self.profits.items()) if pair[0] > 0]))
        return self._profited_profits

    def _combine_sim_runners(self, sim_runners):
        for sim_runner in sim_runners:
            self.profits += sim_runner.results_data.profits
            self.profited_terminal_bets += sim_runner.results_data.profited_terminal_bets
            self.busted_terminal_bets += sim_runner.results_data.busted_terminal_bets
            self.profited_bet_counts += sim_runner.results_data.profited_bet_counts
            self.busted_bet_counts += sim_runner.results_data.busted_bet_counts
            self.profited_bankrolls += sim_runner.results_data.profited_bankrolls
            self.busted_bankrolls += sim_runner.results_data.busted_bankrolls
            if len(sim_runner.results_data.longest_win_series_events) > len(self.longest_win_series_events):
                self.longest_win_series_events = sim_runner.results_data.longest_win_series_events
            if len(sim_runner.results_data.longest_lose_series_events) > len(self.longest_lose_series_events):
                self.longest_lose_series_events = sim_runner.results_data.longest_lose_series_events
