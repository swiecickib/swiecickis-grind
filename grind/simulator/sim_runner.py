from random import randrange
from grind.simulator.sim_results import SimResults


class SimRunner:
    def __init__(self, input_params):
        self.params = input_params
        self.results_data = SimResults()
        self._wheel_slots = 38 if input_params.table_type == 'american' else 37
        self._events = list()
        self._bet = self.params.min_bet
        self._bankroll = self._bet * -1
        self._profit = self._bet * -1
        self._busted = False
        self._profited = False

    def run_sims(self, sim_count):
        for _ in range(sim_count):
            self._simulate_grind()

    def _simulate_grind(self):
        self._reinitialize_sim_vars()
        bet_count = 0
        while not self._busted and not self._profited:
            bet_count += 1
            winning_number = randrange(0, self._wheel_slots + 1)
            if winning_number < self.params.winning_positions:
                self._handle_win()
            else:
                self._handle_loss()
        self._update_results(bet_count)

    def _reinitialize_sim_vars(self):
        self._events = list()
        self._bet = self.params.min_bet
        self._bankroll = self._bet * -1
        self._profit = self._bet * -1
        self._busted = False
        self._profited = False

    def _handle_win(self):
        self._events.append((self._bet, 'won'))
        self._profit += (self.params.payout_factor * self._bet)
        if self._profit > 0:
            self._profited = True
        else:
            self._bet = self._get_next_bet_on_win(self._bet, self._profit)

    def _handle_loss(self):
        self._events.append((self._bet, 'lost'))
        self._profit -= self._bet
        if self._profit < self._bankroll:
            self._bankroll = self._profit
        if self.params.bankrupt_at_max_bet:
            if self._bet == self.params.max_bet:
                self._busted = True
        else:
            if abs(self._bankroll) >= self.params.bankrupt_level:
                self._busted = True
            elif abs(self._bankroll) + self._bet > self.params.bankrupt_level:
                self._bet = self.params.bankrupt_level - abs(self._bankroll)

    def _get_next_bet_on_win(self, prev_bet, profit):
        next_bet = min(self.params.max_bet, prev_bet + self.params.min_bet_increment)
        if not self.params.bankrupt_at_max_bet:
            max_bet = min(self.params.bankrupt_level - abs(profit), self.params.max_bet)
        else:
            max_bet = self.params.max_bet
        next_bet = min(max_bet, next_bet)
        if profit + (self.params.payout_factor * next_bet) > min(self.params.min_bet, self.params.min_bet_increment):
            inc_factor = ((((profit * -1)/self.params.payout_factor) - self.params.min_bet)
                          // self.params.min_bet_increment) + 1
            next_bet = self.params.min_bet + (self.params.min_bet_increment * inc_factor)
        return next_bet  # Note that not caching these results had the best performance.

    def _update_results(self, bet_count):
        self.results_data.profits[self._profit] += 1
        if self._profit > 0:
            if len(self._events) > len(self.results_data.longest_win_series_events):
                self.longest_win_series_events = self._events
            self.results_data.profited_bankrolls[abs(self._bankroll)] += 1
            self.results_data.profited_terminal_bets[self._bet] += 1
            self.results_data.profited_bet_counts[bet_count] += 1
        else:
            if len(self._events) > len(self.results_data.longest_lose_series_events):
                self.longest_lose_series_events = self._events
            self.results_data.busted_bankrolls[abs(self._bankroll)] += 1
            self.results_data.busted_terminal_bets[self._bet] += 1
            self.results_data.busted_bet_counts[bet_count] += 1
