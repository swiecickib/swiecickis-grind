import logging
import multiprocessing as mp
import time
from grind.simulator.sim_results import SimResults
from grind.simulator.sim_runner import SimRunner


class GrindSimulator:
    def __init__(self, input_params):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.params = input_params
        self.sim_results = None

    def simulate(self):
        self.logger.info("Currently running {} simulations of Oscar's grind...".format(self.params.sim_count))
        start_time = time.time()
        sims_per_proc, remainder = divmod(self.params.sim_count, self.params.processes)
        sims_per_proc_list = [sims_per_proc] * self.params.processes
        processes = list()
        results_queue = mp.Queue()
        for i in range(remainder):
            sims_per_proc_list[i] += 1
        for sim_count in sims_per_proc_list:
            p = mp.Process(target=self._run_simulations, args=(sim_count, results_queue,))
            p.start()
            processes.append(p)
        sim_runners = list()
        for _ in range(len(processes)):
            sim_runners.append(results_queue.get())
        for p in processes:
            p.join()
        self.sim_results = SimResults(sim_runners=sim_runners)
        self.logger.info("Completed running {} simulations of Oscar's grind in {:.3f} seconds."
                         .format(self.params.sim_count, time.time() - start_time))

    def _run_simulations(self, sim_count, results_queue):
        sim_runner = SimRunner(self.params)
        sim_runner.run_sims(sim_count)
        results_queue.put(sim_runner)
