import os
from pathlib import Path
from reporter.reporter import Reporter
import pandas as pd
import plotly.express as px


class SimReporter(Reporter):
    def __init__(self, simulator):
        self.params = simulator.params
        self.sim_results = simulator.sim_results
        self.template_vars = dict()
        Reporter.__init__(self, "grind/report_template.html")

    def _get_template_vars(self):
        self._add_input_parameters()
        self._add_summary_statistics()
        self._add_plots()
        if self.params.bankrupt_at_max_bet:
            self._add_losses_plot_and_profits_table()
        else:
            self._add_combined_profits_table()
        return self.template_vars

    def _add_input_parameters(self):
        self.template_vars.update({
            'strategy': self.params.strategy,
            'table_type': self.params.table_type,
            'min_bet': self.params.min_bet,
            'max_bet': self.params.max_bet,
            'min_bet_increment': self.params.min_bet_increment,
            'winning_positions': self.params.winning_positions,
            'payout_factor': self.params.payout_factor,
            'sim_count': self.params.sim_count,
            'bankrupt_at_max_bet': self.params.bankrupt_at_max_bet,
            'bankrupt_level': self.params.bankrupt_level
        })

    def _add_summary_statistics(self):
        self.template_vars.update({
            'profited_count': sum(self.sim_results.profited_profits.values()),
            'busted_count': sum(self.sim_results.busted_profits.values()),
        })
        self.template_vars.update({
            'p_profited': self.template_vars['profited_count'] / self.template_vars['sim_count'],
            'p_busted': self.template_vars['busted_count'] / self.template_vars['sim_count'],
            'average_profit': sum(number * count for number, count in self.sim_results.profited_profits.items()) / self.template_vars['profited_count'],
            'average_loss': sum(number * count for number, count in self.sim_results.busted_profits.items()) / self.template_vars['busted_count'],
        })
        self.template_vars.update({
            'net_ev': (self.template_vars['p_profited'] * self.template_vars['average_profit']) +
                      (self.template_vars['p_busted'] * self.template_vars['average_loss']),
            'largest_profited_bankroll': max(self.sim_results.profited_bankrolls),
            'largest_busted_bankroll': max(self.sim_results.busted_bankrolls)
        })

    def _add_plots(self):
        self.template_vars.update({
            'bankrolls_bars_plot':
                self._make_bars_plot('bankroll', self.sim_results.profited_bankrolls,
                                     self.sim_results.busted_bankrolls),
            'bet_counts_bars_plot':
                self._make_bars_plot('bets', self.sim_results.profited_bet_counts, self.sim_results.busted_bet_counts),
            'bankrolls_box_plot':
                self._make_box_plot('bankrolls', self.sim_results.profited_bankrolls,
                                    self.sim_results.busted_bankrolls),
            'bet_counts_box_plot':
                self._make_box_plot('bet counts', self.sim_results.profited_bet_counts,
                                    self.sim_results.busted_bet_counts),
            'terminal_bets_bars_plot': self._make_bars_plot('terminal bet', self.sim_results.profited_terminal_bets,
                                                            self.sim_results.busted_terminal_bets),
            'terminal_bets_box_plot': self._make_box_plot('terminal bets', self.sim_results.profited_terminal_bets,
                                                          self.sim_results.busted_terminal_bets)
        })

    def _add_combined_profits_table(self):
        profits_df = pd.DataFrame.from_dict(self.sim_results.profits, orient='index').reset_index() \
            .rename(columns={'index': 'profit', 0: 'count'}).sort_values('profit')
        self.template_vars.update({
            'combined_profits_table': profits_df.to_html(index=False, col_space=80).replace('border="1"', 'border="0"')
        })

    def _add_losses_plot_and_profits_table(self):
        profits_df = pd.DataFrame.from_dict(self.sim_results.profited_profits, orient='index').reset_index() \
            .rename(columns={'index': 'Profit', 0: 'Count'}).sort_values('Profit')
        losses_df = pd.DataFrame({'loss': list(self.sim_results.busted_profits.elements())})
        box_plot_fig = px.box(losses_df, y='loss')
        box_plot_fig.update_layout(paper_bgcolor='aliceblue', title_text='Losses Box Plot', title_x=0.5)
        self.template_vars.update({
            'profits_table': profits_df.to_html(index=False, col_space=80).replace('border="1"', 'border="0"'),
            'losses_box_plot': box_plot_fig.to_html(full_html=False)
        })

    @staticmethod
    def _make_bars_plot(item_str, profited_counter, busted_counter, log_x=True):
        profited_df = pd.DataFrame.from_dict(profited_counter, orient='index') \
            .reset_index().rename(columns={'index': item_str, 0: 'count'})
        profited_df['Outcome'] = 'profited'
        busted_df = pd.DataFrame.from_dict(busted_counter, orient='index') \
            .reset_index().rename(columns={'index': item_str, 0: 'count'})
        busted_df['Outcome'] = 'busted'
        bars_fig = px.bar(pd.concat([profited_df, busted_df]), x=item_str, y='count', color='Outcome', log_x=log_x)
        bars_fig.update_layout(paper_bgcolor='aliceblue', title_text='Bar Chart', title_x=0.5)
        return bars_fig.to_html(full_html=False)

    @staticmethod
    def _make_box_plot(item_str, profited_counter, busted_counter):
        profited_df = pd.DataFrame({item_str: list(profited_counter.elements())})
        profited_df['Outcome'] = 'profited'
        busted_df = pd.DataFrame({item_str: list(busted_counter.elements())})
        busted_df['Outcome'] = 'busted'
        df = pd.concat([profited_df, busted_df])
        combined_df = df.copy()
        combined_df['Outcome'] = 'combined'
        box_plot_fig = px.box(pd.concat([df, combined_df]), y=item_str, log_y=True, color='Outcome')
        box_plot_fig.update_layout(paper_bgcolor='aliceblue', title_text='Box Plot', title_x=0.5)
        return box_plot_fig.to_html(full_html=False)

    def _get_output_filename(self):
        file_name = "{strategy}_{table_type}_{min_bet}_{max_bet}_{min_bet_increment}_{winning_positions}_" \
                    "{payout_factor}_sim_count_{sim_count}_bankrupt_at_max_bet_{bankrupt_at_max_bet}_" \
                    "bankrupt_level_{bankrupt_level}.html" \
            .format(strategy=self.params.strategy,
                    table_type=self.params.table_type,
                    min_bet=self.params.min_bet,
                    max_bet=self.params.max_bet,
                    min_bet_increment=self.params.min_bet_increment,
                    winning_positions=self.params.winning_positions,
                    payout_factor=self.params.payout_factor,
                    sim_count=self.params.sim_count,
                    bankrupt_at_max_bet=self.params.bankrupt_at_max_bet,
                    bankrupt_level=self.params.bankrupt_level
                    )
        return os.path.join(Path(__file__).parents[2], 'reports', file_name)
