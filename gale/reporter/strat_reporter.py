import os
import numpy as np
import pandas as pd
from pathlib import Path
from reporter.reporter import Reporter


class StratReporter(Reporter):
    def __init__(self, strat_maker):
        self.strat_maker = strat_maker
        Reporter.__init__(self, "gale/report_template.html")

    def _get_template_vars(self):
        five_perc_loss_threshold_stats = \
            self.strat_maker.best_win_ev_sequence.get_runs_and_winnings_given_total_loss_threshold(.05)
        ten_perc_loss_threshold_stats = \
            self.strat_maker.best_win_ev_sequence.get_runs_and_winnings_given_total_loss_threshold(.10)
        template_vars = {
            'strategy': self.strat_maker.params.strategy,
            'table_type': self.strat_maker.params.table_type,
            'min_bet': self.strat_maker.params.min_bet,
            'max_bet': self.strat_maker.params.max_bet,
            'min_bet_increment': self.strat_maker.params.min_bet_increment,
            'winning_positions': self.strat_maker.params.winning_positions,
            'payout_factor': self.strat_maker.params.payout_factor,
            'bet_sequence_table': self._get_bet_sequence_df_html(),
            'required_bankroll': self.strat_maker.best_win_ev_sequence.bets[-1].cumulative_stake,
            'p_lose_all': self.strat_maker.best_win_ev_sequence.get_p_total_loss(),
            'win_ev_sum': self.strat_maker.best_win_ev_sequence.win_ev_sum,
            'net_ev': self.strat_maker.best_win_ev_sequence.get_net_ev(),
            'five_perc_loss_true_p': five_perc_loss_threshold_stats[0],
            'five_perc_loss_runs': five_perc_loss_threshold_stats[1],
            'five_perc_loss_exp_win': five_perc_loss_threshold_stats[2],
            'ten_perc_loss_true_p': ten_perc_loss_threshold_stats[0],
            'ten_perc_loss_runs': ten_perc_loss_threshold_stats[1],
            'ten_perc_loss_exp_win': ten_perc_loss_threshold_stats[2],
        }
        return template_vars

    def _get_bet_sequence_df_html(self):
        p_win_on_roll = [self.strat_maker.best_win_ev_sequence.bet_universe.probability_dict[i]['p_win'] for i in
                         range(len(self.strat_maker.best_win_ev_sequence))]
        df_data = {
            'Roll': [i for i in range(1, len(self.strat_maker.best_win_ev_sequence.bets) + 1)],
            'Stake': [bet.stake for bet in self.strat_maker.best_win_ev_sequence.bets],
            'Cumulative Stake': [bet.cumulative_stake for bet in self.strat_maker.best_win_ev_sequence.bets],
            'Profit': [bet.profit for bet in self.strat_maker.best_win_ev_sequence.bets],
            'Win EV': [bet.win_ev for bet in self.strat_maker.best_win_ev_sequence.bets],
            'p(win this roll)': p_win_on_roll,
            'p(win on or before this roll)': np.cumsum(p_win_on_roll)
        }
        df = pd.DataFrame(df_data)
        return df.to_html(index=False, col_space=80).replace('border="1"', 'border="0"')

    def _get_output_filename(self):
        file_name = "{strategy}_{table_type}_{min_bet}_{max_bet}_{min_bet_increment}_{winning_positions}_" \
                    "{payout_factor}.html".format(strategy=self.strat_maker.params.strategy,
                                                  table_type=self.strat_maker.params.table_type,
                                                  min_bet=self.strat_maker.params.min_bet,
                                                  max_bet=self.strat_maker.params.max_bet,
                                                  min_bet_increment=self.strat_maker.params.min_bet_increment,
                                                  winning_positions=self.strat_maker.params.winning_positions,
                                                  payout_factor=self.strat_maker.params.payout_factor
                                                  )
        return os.path.join(Path(__file__).parents[2], 'reports', file_name)
