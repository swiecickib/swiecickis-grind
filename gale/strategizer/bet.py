class Bet:
    def __init__(self, roll, stake, cumulative_stake, profit, win_ev):
        self.roll = roll
        self.stake = stake
        self.cumulative_stake = cumulative_stake
        self.profit = profit
        self.win_ev = win_ev

    def __eq__(self, other):
        if self.roll != other.roll:
            return False
        if self.stake != other.stake:
            return False
        if self.cumulative_stake != other.cumulative_stake:
            return False
        return True
