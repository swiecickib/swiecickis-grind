import copy
import logging
import time
from gale.strategizer.bet_sequence import BetSequence
from gale.strategizer.bet_universe import BetUniverse


class StratMaker:
    def __init__(self, input_params):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.params = input_params
        self.min_profitability_bet_sequence = None
        self.best_win_ev_sequence = None

    def compute_optimal_strat(self):
        self.logger.info("Currently computing optimal modified martingale betting strategy...")
        start_time = time.time()
        min_profitability_bet_vals = self._get_min_profitability_bets()
        p_win_single_roll = self.params.winning_positions / (38 if self.params.table_type == 'american' else 37)
        bet_universe = BetUniverse(len(min_profitability_bet_vals), p_win_single_roll, self.params.payout_factor)
        self.min_profitability_bet_sequence = \
            BetSequence(bet_universe, self.params.payout_factor, self.params.max_bet,
                        self.params.min_bet_increment, input_bets=min_profitability_bet_vals)
        self.logger.debug("The min profitability bet sequence is {}".format(self.min_profitability_bet_sequence))
        self.best_win_ev_sequence = self.min_profitability_bet_sequence
        # TODO: Make this faster by first approximating the optimal solution with a larger bet increment.
        self._find_optimal_strat_by_incrementing_recursively()
        self.logger.info("Finished computing optimal modified martingale betting strategy in {:.3f} seconds."
                         .format(time.time() - start_time))

    def _find_optimal_strat_by_incrementing_recursively(self):
        for starting_idx in range(len(self.min_profitability_bet_sequence) - 2, -1, -1):
            self._increment_bets_recursively(copy.copy(self.min_profitability_bet_sequence), starting_idx)
            self.logger.debug("Finished attempting to find better solutions by incrementing the bet in the {} position "
                              "(zero indexed).".format(starting_idx))
            if self.min_profitability_bet_sequence[starting_idx].stake == self.best_win_ev_sequence[starting_idx].stake:
                self.logger.debug("The best bet sequence was not updated after attempting to increment the bet in the "
                                  "{} position (zero indexed) so solution finding will commence.".format(starting_idx))
                break

    def _increment_bets_recursively(self, bet_sequence, bet_idx):
        while bet_idx < len(bet_sequence) and bet_sequence[bet_idx].stake < self.params.max_bet:
            if bet_sequence.update_bet_stake(bet_idx, bet_sequence[bet_idx].stake + self.params.min_bet_increment):
                if bet_sequence.win_ev_sum > self.best_win_ev_sequence.win_ev_sum:
                    self._update_best_bet_sequence(bet_sequence)
                self._increment_bets_recursively(copy.copy(bet_sequence), bet_idx + 1)

    def _update_best_bet_sequence(self, bet_sequence):
        self.logger.debug("Updating the best bet sequence found from {} to {}."
                          .format(self.best_win_ev_sequence, bet_sequence))
        self.best_win_ev_sequence = copy.copy(bet_sequence)

    def _get_min_profitability_bets(self):
        min_profitability_bets = [self.params.min_bet]
        cumulative_stake = self.params.min_bet
        potential_bet = self.params.min_bet
        while potential_bet <= self.params.max_bet:
            potential_bet_profit = (potential_bet * (self.params.payout_factor + 1)) - (cumulative_stake + potential_bet)
            if potential_bet_profit > 0:
                min_profitability_bets.append(potential_bet)
                cumulative_stake += potential_bet
            else:
                if potential_bet_profit != 0:
                    increment_factor = \
                        ((potential_bet_profit * -1) // (self.params.min_bet_increment * self.params.payout_factor)) + 1
                    potential_bet += increment_factor * self.params.min_bet_increment
                else:
                    potential_bet += self.params.min_bet_increment
        min_profitability_bets[-1] = self.params.max_bet
        return min_profitability_bets
