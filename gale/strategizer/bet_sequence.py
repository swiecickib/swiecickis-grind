import math


class BetSequence:
    def __init__(self, bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=None):
        self.bet_universe = bet_universe
        self.payout_factor = payout_factor
        self.max_bet = max_bet
        self.min_bet_increment = min_bet_increment
        self.bets = list()
        self.win_ev_sum = 0
        if isinstance(input_bets, list):
            for bet_val in input_bets:
                self.add_bet_val(bet_val)

    def __copy__(self):
        cls = self.__class__
        result = cls.__new__(cls)
        setattr(result, 'bet_universe', self.bet_universe)
        setattr(result, 'payout_factor', self.payout_factor)
        setattr(result, 'max_bet', self.max_bet)
        setattr(result, 'min_bet_increment', self.min_bet_increment)
        setattr(result, 'bets', [*self.bets])
        setattr(result, 'win_ev_sum', self.win_ev_sum)
        return result

    def __eq__(self, other):
        return self.bets == other.bets

    def __len__(self):
        return len(self.bets)

    def __getitem__(self, idx):
        return self.bets[idx]

    def __str__(self):
        print_dict = dict()
        print_dict["Win EV Sum"] = self.win_ev_sum
        print_dict["Net EV"] = self.get_net_ev()
        print_dict["Cumulative Stake"] = self.bets[-1].cumulative_stake
        print_dict["Probability of total loss"] = self.get_p_total_loss()
        print_dict["Bets"] = [bet.stake for bet in self.bets]
        return str(print_dict)

    def add_bet_val(self, bet_val):
        if not self.bets:
            cumulative_stake = bet_val
        else:
            cumulative_stake = self.bets[-1].cumulative_stake + bet_val
        bet = self.bet_universe[(len(self.bets), bet_val, cumulative_stake)]
        self.bets.append(bet)
        self.win_ev_sum += bet.win_ev

    def get_net_ev(self):
        return self.win_ev_sum - (self.get_p_total_loss() * self.bets[-1].cumulative_stake)

    def get_p_total_loss(self):
        return self.bet_universe.probability_dict[len(self.bets) - 1]['p_loss']

    def get_runs_and_winnings_given_total_loss_threshold(self, p_total_loss_desired):
        if p_total_loss_desired < self.get_p_total_loss():
            true_loss_probability = self.get_p_total_loss()
            runs = 1
            exp_win = self.win_ev_sum
        else:
            p_win_single_sequence = 1 - self.get_p_total_loss()
            runs = math.ceil(math.log(1 - p_total_loss_desired)/math.log(p_win_single_sequence))
            true_loss_probability = 1 - (p_win_single_sequence ** runs)
            exp_win = runs * self.win_ev_sum
        return true_loss_probability, runs, exp_win

    def update_bet_stake(self, bet_idx, new_stake):
        if new_stake > self.max_bet:
            self.bets.clear()
            return False
        cumulative_stake = self.bets[bet_idx].cumulative_stake + (new_stake - self.bets[bet_idx].stake)
        self._update_bet(bet_idx, new_stake, cumulative_stake)
        return self._cascade_updates_to_following_bets(bet_idx + 1)

    def _cascade_updates_to_following_bets(self, starting_idx):
        for bet_idx in range(starting_idx, len(self.bets)):
            cumulative_stake = self.bets[bet_idx - 1].cumulative_stake + self.bets[bet_idx].stake
            bet_profit = (self.bets[bet_idx].stake * (self.payout_factor + 1)) - cumulative_stake
            if bet_profit <= 0:
                increment_factor = ((bet_profit * -1) // (self.min_bet_increment * self.payout_factor)) + 1
                stake_increase = increment_factor * self.min_bet_increment
                stake = self.bets[bet_idx].stake + stake_increase
                if stake > self.max_bet:
                    self.bets.clear()
                    return False
                cumulative_stake = cumulative_stake + stake_increase
            else:
                stake = self.bets[bet_idx].stake
            self._update_bet(bet_idx, stake, cumulative_stake)
        return True

    def _update_bet(self, bet_idx, stake, cumulative_stake):
        old_win_ev = self.bets[bet_idx].win_ev
        self.bets[bet_idx] = self.bet_universe[(bet_idx, stake, cumulative_stake)]
        self.win_ev_sum += (self.bets[bet_idx].win_ev - old_win_ev)
