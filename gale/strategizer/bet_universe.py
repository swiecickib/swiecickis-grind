from gale.strategizer.bet import Bet


class BetUniverse:
    def __init__(self, max_rolls, p_win_single_roll, payout_factor):
        self.bet_dict = dict()
        self.probability_dict = dict()
        self.payout_factor = payout_factor
        self._initialize_probability_dict(max_rolls, p_win_single_roll)

    def _initialize_probability_dict(self, max_rolls, p_win_single_roll):
        self.probability_dict[0] = {'p_win': p_win_single_roll}
        self.probability_dict[0]['p_loss'] = 1 - p_win_single_roll
        for roll in range(1, max_rolls):
            self.probability_dict[roll] = \
                {'p_win': self.probability_dict[0]['p_win'] * self.probability_dict[roll - 1]['p_loss']}
            self.probability_dict[roll]['p_loss'] = \
                self.probability_dict[0]['p_loss'] * self.probability_dict[roll - 1]['p_loss']

    def __getitem__(self, item):
        if item not in self.bet_dict:
            roll = item[0]
            stake = item[1]
            cumulative_stake = item[2]
            profit = (stake * (self.payout_factor + 1)) - cumulative_stake
            win_ev = profit * self.probability_dict[roll]['p_win']
            self.bet_dict[item] = Bet(roll, stake, cumulative_stake, profit, win_ev)
        return self.bet_dict[item]
