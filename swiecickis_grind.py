import logging
import os
from datetime import datetime
from initializer.init_helper import InitHelper
from gale.strategizer.strat_maker import StratMaker
from gale.reporter.strat_reporter import StratReporter
from grind.reporter.sim_reporter import SimReporter
from grind.simulator.grind_simulator import GrindSimulator


def main():
    init_helper = InitHelper()
    init_helper.initialize()
    setup_logging(init_helper.params.log)
    if init_helper.params.strategy == 'gale':
        strat_maker = StratMaker(init_helper.params)
        print("The application is currently building the betting strategy...")
        strat_maker.compute_optimal_strat()
        reporter = StratReporter(strat_maker)
    else:
        grind_simulator = GrindSimulator(init_helper.params)
        print("The application is currently running simulations...")
        grind_simulator.simulate()
        reporter = SimReporter(grind_simulator)
    reporter.generate_report()
    print("Application processing completed successfully. The output report can be found at:\n{}"
          .format(reporter.output_filename))


def setup_logging(log_level_str):
    filename = "{}{}{}.log".format(datetime.now().year, datetime.now().month, datetime.now().day)
    log_file = os.path.join(os.path.dirname(__file__), 'logs', filename)
    logging.basicConfig(
        filename=log_file,
        level=getattr(logging, log_level_str),
        datefmt='%Y-%m-%d %H:%M:%S',
        format='[%(asctime)s] [%(levelname)s] %(name)s:%(funcName)s:%(lineno)d - %(message)s'
    )


if __name__ == '__main__':
    main()
