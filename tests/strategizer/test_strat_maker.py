from argparse import Namespace
from pytest import approx
from gale.strategizer.strat_maker import StratMaker


def test_compute_known_optimal():
    params = Namespace(
        strategy='gale',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2
    )
    sm = StratMaker(params)
    sm.compute_optimal_strat()
    assert sm.best_win_ev_sequence.win_ev_sum == approx(1.939270697)
    assert sm.best_win_ev_sequence[0].stake == 1
    assert sm.best_win_ev_sequence[1].stake == 1
    assert sm.best_win_ev_sequence[2].stake == 2
    assert sm.best_win_ev_sequence[3].stake == 3
    assert sm.best_win_ev_sequence[4].stake == 4
    assert sm.best_win_ev_sequence[5].stake == 6
    assert sm.best_win_ev_sequence[6].stake == 9
    assert sm.best_win_ev_sequence[7].stake == 23
    assert sm.best_win_ev_sequence[8].stake == 25
