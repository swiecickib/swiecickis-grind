import copy
from pytest import approx
from gale.strategizer.bet_sequence import BetSequence
from gale.strategizer.bet_universe import BetUniverse


def get_bet_sequence_standard(max_bet=9, max_rolls=7):
    p_win_single_roll = 12 / 37
    payout_factor = 2
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    return BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=min_profitability_bet_vals)


def test_init_win_ev_sum_and_bet_addition():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    bet_universe = BetUniverse(7, p_win_single_roll, payout_factor)
    max_bet = 9
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    bet_sequence = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment,
                               input_bets=min_profitability_bet_vals)
    for idx, bet in enumerate(bet_sequence):
        assert bet.stake == min_profitability_bet_vals[idx]
    assert bet_sequence.win_ev_sum == approx(1.508141709)


def test_shallow_copy_impl():
    bs1 = get_bet_sequence_standard()
    bs2 = copy.copy(bs1)
    assert bs1.bet_universe == bs2.bet_universe
    assert bs1.payout_factor == bs2.payout_factor
    assert bs1.max_bet == bs2.max_bet
    assert bs1.min_bet_increment == bs2.min_bet_increment
    assert bs1.win_ev_sum == bs2.win_ev_sum
    for idx, bet in enumerate(bs1):
        assert id(bet) == id(bs2[idx])


def test_equality_impl_w_shallow_copy():
    bs1 = get_bet_sequence_standard()
    bs2 = copy.copy(bs1)
    assert bs1 == bs2


def test_equality_impl_diff_bet_ids():
    bs1 = get_bet_sequence_standard()
    max_bet = 9
    max_rolls = 7
    p_win_single_roll = 12 / 37
    payout_factor = 2
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    bs2 = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=min_profitability_bet_vals)
    assert bs1 == bs2


def test_len_impl():
    bs = get_bet_sequence_standard()
    assert len(bs) == 7


def test_getitem_impl():
    bs = get_bet_sequence_standard()
    assert id(bs[2]) == id(bs.bet_universe[(2, 2, 4)])


def test_add_bet_val():
    bs = get_bet_sequence_standard(max_bet=14, max_rolls=8)
    bs.add_bet_val(14)
    assert len(bs) == 8
    assert id(bs[7]) == id(bs.bet_universe[(7, 14, 40)])
    assert bs[-1].profit == 2
    assert bs[-1].win_ev == approx(0.041703981)
    assert bs.win_ev_sum == approx(1.54984569)


def test_get_net_ev():
    bs = get_bet_sequence_standard()
    assert bs.get_net_ev() == approx(-0.163492847)


def test_get_runs_and_winnings_given_total_loss_threshold_not_possible():
    bs = get_bet_sequence_standard()
    true_loss_probability, runs, exp_win = bs.get_runs_and_winnings_given_total_loss_threshold(.02)
    assert true_loss_probability == approx(bs.bet_universe.probability_dict[len(bs) - 1]['p_loss'])
    assert runs == 1
    assert exp_win == approx(bs.win_ev_sum)


def test_get_runs_and_winnings_given_total_loss_threshold_possible():
    bs = get_bet_sequence_standard()
    true_loss_probability, runs, exp_win = bs.get_runs_and_winnings_given_total_loss_threshold(.10)
    assert true_loss_probability == approx(0.124453601)
    assert runs == 2
    assert exp_win == approx(3.016283417)


def test_update_bet_stake_happy_case_1_to_1():
    p_win_single_roll = 18 / 37
    payout_factor = 1
    max_rolls = 5
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 24
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 2, 4, 8, 16]
    bet_sequence = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment,
                               input_bets=min_profitability_bet_vals)
    bet_sequence.update_bet_stake(0, 2)
    assert bet_sequence[0].stake == 2
    assert bet_sequence[0].win_ev == approx(0.972972973)
    assert bet_sequence[1].stake == 3
    assert bet_sequence[1].win_ev == approx(0.249817385)
    assert bet_sequence[2].stake == 6
    assert bet_sequence[2].win_ev == approx(0.128284603)
    assert bet_sequence[3].stake == 12
    assert bet_sequence[3].win_ev == approx(0.065875877)
    assert bet_sequence[4].stake == 24
    assert bet_sequence[4].win_ev == approx(0.033828153)
    assert bet_sequence.win_ev_sum == approx(1.450778991)


def test_update_bet_stake_happy_case_2_to_1():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=min_profitability_bet_vals)
    bs.update_bet_stake(0, 2)
    assert bs[0].stake == 2
    assert bs[0].win_ev == approx(1.297297297)
    assert bs[1].stake == 2
    assert bs[1].win_ev == approx(0.438276114)
    assert bs[2].stake == 3
    assert bs[2].win_ev == approx(0.296132509)
    assert bs[3].stake == 4
    assert bs[3].win_ev == approx(0.100044767)
    assert bs[4].stake == 6
    assert bs[4].win_ev == approx(0.067597815)
    assert bs[5].stake == 9
    assert bs[5].win_ev == approx(0.0456742)
    assert bs[6].stake == 14
    assert bs[6].win_ev == approx(0.061721891)
    assert bs.win_ev_sum == approx(2.306744594)


def test_update_bet_stake_beyond_max():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=min_profitability_bet_vals)
    assert bs.update_bet_stake(0, 15) is False
    assert len(bs) == 0


def test_update_bet_stake_with_clear():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    min_profitability_bet_vals = [1, 1, 2, 3, 4, 6, 9]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=min_profitability_bet_vals)
    assert bs.update_bet_stake(0, 9) is False
    assert len(bs) == 0


def test_update_bet_stake_no_cascading_updates():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    input_bets = [1, 1, 2, 3, 4, 10, 12]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=input_bets)
    assert bs.update_bet_stake(4, 5) is True
    assert bs[4].stake == 5
    assert bs[4].cumulative_stake == 12
    assert bs[4].win_ev == approx(0.202793446)
    assert bs[5].stake == 10
    assert bs[5].cumulative_stake == 22
    assert bs[5].win_ev == approx(0.365393596)
    assert bs[6].stake == 12
    assert bs[6].cumulative_stake == 34
    assert bs[6].win_ev == approx(0.061721891)
    assert bs.win_ev_sum == approx(1.993917682)


def test_update_bet_stake_not_impending_cascading_update_due_on_final():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    input_bets = [1, 1, 2, 3, 4, 10, 11]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=input_bets)
    assert bs.update_bet_stake(4, 5) is True
    assert bs[4].stake == 5
    assert bs[4].cumulative_stake == 12
    assert bs[4].win_ev == approx(0.202793446)
    assert bs[5].stake == 10
    assert bs[5].cumulative_stake == 22
    assert bs[5].win_ev == approx(0.365393596)
    assert bs[6].stake == 12
    assert bs[6].cumulative_stake == 34
    assert bs[6].win_ev == approx(0.061721891)
    assert bs.win_ev_sum == approx(1.993917682)


def test_update_bet_stake_last_index():
    p_win_single_roll = 12 / 37
    payout_factor = 2
    max_rolls = 7
    bet_universe = BetUniverse(max_rolls, p_win_single_roll, payout_factor)
    max_bet = 14
    min_bet_increment = 1
    input_bets = [1, 1, 2, 3, 5, 10, 11]
    bs = BetSequence(bet_universe, payout_factor, max_bet, min_bet_increment, input_bets=input_bets)
    assert bs.update_bet_stake(6, 13) is True
    assert bs[6].stake == 13
    assert bs[6].cumulative_stake == 35
    assert bs[6].profit == 4
    assert bs[6].win_ev == approx(0.123443783)
    assert bs.win_ev_sum == approx(2.055639573)
