from pytest import approx
from gale.strategizer.bet import Bet
from gale.strategizer.bet_universe import BetUniverse

MAX_ROLLS = 7
P_WIN_SINGLE_ROLL = 12 / 37
P_LOSE_SINGLE_ROLL = 1 - P_WIN_SINGLE_ROLL
PAYOUT_FACTOR = 2


def get_bet_universe():
    return BetUniverse(MAX_ROLLS, P_WIN_SINGLE_ROLL, PAYOUT_FACTOR)


def test_probability_dict_len():
    bet_universe = get_bet_universe()
    assert len(bet_universe.probability_dict) == MAX_ROLLS


def test_probability_dict_first_roll():
    bet_universe = get_bet_universe()
    assert bet_universe.probability_dict[0]['p_win'] == P_WIN_SINGLE_ROLL
    assert bet_universe.probability_dict[0]['p_loss'] == 1 - P_WIN_SINGLE_ROLL


def test_probability_dict_third_roll():
    bet_universe = get_bet_universe()
    assert bet_universe.probability_dict[2]['p_win'] == P_WIN_SINGLE_ROLL * (P_LOSE_SINGLE_ROLL ** 2)
    assert bet_universe.probability_dict[2]['p_loss'] == P_LOSE_SINGLE_ROLL ** 3


def test_probability_dict_last_roll():
    bet_universe = get_bet_universe()
    assert bet_universe.probability_dict[MAX_ROLLS - 1]['p_loss'] == \
           approx(P_LOSE_SINGLE_ROLL ** MAX_ROLLS)
    assert bet_universe.probability_dict[MAX_ROLLS - 1]['p_win'] == \
           approx(P_WIN_SINGLE_ROLL * (P_LOSE_SINGLE_ROLL ** (MAX_ROLLS- 1)))


def test_bet_dict_item_addition():
    bet_universe = get_bet_universe()
    roll = 5
    stake = 6
    cumulative_stake = 17
    bet = bet_universe[(roll, stake, cumulative_stake)]
    assert bet_universe[(roll, stake, cumulative_stake)] == bet


def test_bet_dict_addition_with_no_duplicate():
    bet_universe = get_bet_universe()
    roll = 5
    stake = 6
    cumulative_stake = 17
    bet = bet_universe[(roll, stake, cumulative_stake)]
    same_bet = bet_universe[(roll, stake, cumulative_stake)]
    assert id(bet) == id(same_bet)
    assert len(bet_universe.bet_dict) == 1


def test_bet_dict_correct_bet_attributes():
    bet_universe = get_bet_universe()
    roll = 5
    stake = 6
    cumulative_stake = 17
    bet_from_uni = bet_universe[(roll, stake, cumulative_stake)]
    profit = (stake * (PAYOUT_FACTOR + 1)) - cumulative_stake
    win_ev = profit * P_WIN_SINGLE_ROLL * (P_LOSE_SINGLE_ROLL ** 5)
    bet_from_test = Bet(roll, stake, cumulative_stake, profit, win_ev)
    assert bet_from_test == bet_from_uni
    assert bet_universe[(roll, stake, cumulative_stake)].roll == bet_from_test.roll
    assert bet_universe[(roll, stake, cumulative_stake)].stake == bet_from_test.stake
    assert bet_universe[(roll, stake, cumulative_stake)].cumulative_stake == bet_from_test.cumulative_stake
    assert bet_universe[(roll, stake, cumulative_stake)].profit == bet_from_test.profit
    assert bet_universe[(roll, stake, cumulative_stake)].win_ev == bet_from_test.win_ev
