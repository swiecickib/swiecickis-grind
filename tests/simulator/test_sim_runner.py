from argparse import Namespace
from grind.simulator.sim_runner import SimRunner


def test_get_next_bet_on_win_normal_incrementation():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=100,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 2
    profit = -20
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 3


def test_get_next_bet_on_win_prev_bet_is_max():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 25
    profit = -200
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 25


def test_get_next_bet_on_win_increment_is_max():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -200
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 25


def test_get_next_bet_on_win_increment_is_max_and_over_budget():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=220,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -200
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 20


def test_get_next_bet_on_win_increment_exceeds_max_and_over_budget():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=220,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 25
    profit = -200
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 20


def test_get_next_bet_on_win_increment_exceeds_max_and_over_budget_but_budget_doesnt_matter():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=220,
        bankrupt_at_max_bet=True
    )
    sr = SimRunner(input_params)
    prev_bet = 25
    profit = -200
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 25


def test_get_next_bet_on_win_increment_is_max_and_too_much_profit_correct_down_one():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -45
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 23


def test_get_next_bet_on_win_increment_is_max_and_too_much_profit_correct_is_same():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -47
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 24


def test_get_next_bet_on_win_increment_is_max_and_too_much_profit_correct_is_min():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -1
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 1


def test_get_next_bet_on_win_increment_is_max_and_too_much_profit_correct_is_down_many():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 24
    profit = -12
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 7


def test_get_next_bet_on_win_too_much_profit_correct_is_min():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 12
    profit = -1
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 1


def test_get_next_bet_on_win_too_much_profit_correct_down_one():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 3
    profit = -3
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 2


def test_get_next_bet_on_win_too_much_profit_correct_down_many_even():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 10
    profit = -11
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 6


def test_get_next_bet_on_win_too_much_profit_correct_down_many_odd():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=1.0,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 10
    profit = -13
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 7


def test_get_next_bet_on_win_too_much_profit_correct_is_under_min_bet():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=1.0,
        max_bet=25.0,
        min_bet_increment=0.25,
        winning_positions=12,
        payout_factor=2,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 20
    profit = -2.25
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 1.25


def test_get_next_bet_on_win_too_much_profit_correct_is_over_min_bet():
    input_params = Namespace(
        strategy='grind',
        table_type='european',
        min_bet=0.5,
        max_bet=25.0,
        min_bet_increment=0.25,
        winning_positions=6,
        payout_factor=5,
        bankrupt_level=500,
        bankrupt_at_max_bet=False
    )
    sr = SimRunner(input_params)
    prev_bet = 20
    profit = -2.75
    next_bet = sr._get_next_bet_on_win(prev_bet, profit)
    assert next_bet == 0.75
